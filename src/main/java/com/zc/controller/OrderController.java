package com.zc.controller;

import com.zc.entity.OrderEnity;
import com.zc.enums.OrderStatusChangeEventEnum;
import com.zc.service.OrderStateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author : ville
 * @version : 1.0.0
 * @description : order控制器
 * @createTime : 2020/6/9-17:44
 * @copyright : villebo@126.com
 * @modify : ville
 **/
@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrderStateService orderStateService;

    /**
     * 列出所有的订单列表
     *
     * @return
     */
    @GetMapping(value = "/orders")
    public ResponseEntity orders() {
       List<OrderEnity> orderList = orderStateService.getOrderList();
        return new ResponseEntity(orderList, HttpStatus.OK);

    }

    /**
     * 通过触发一个事件，改变一个订单的状态
     *
     * @param orderId
     * @param event
     * @return
     */
    @PostMapping(value = "/{orderId}")
    public ResponseEntity processOrderState(@PathVariable("orderId") Integer orderId,
                                            @RequestParam("event") OrderStatusChangeEventEnum event) {
        Boolean result = orderStateService.change(orderId, event);
        return new ResponseEntity(result, HttpStatus.OK);
    }
}
