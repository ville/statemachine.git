package com.zc.util;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.sql.Timestamp;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * @author :  ville
 * @description : 日期操作,基于jdk8日期API封装
 * @CreateTime : 2019-07-05 15:03
 * @Version : 1.0.0
 * @Copyright : http://www.8atour.com
 * @modify : ville
 **/
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DateUtil {
    /**
     * the milli second of a day 24 * 60 * 60 * 1000
     */
    public static final long DAYMILLI = 86400000;
    /**
     * the milli seconds of an hour 60 * 60 * 1000
     */
    public static final long HOURMILLI = 3600000;
    /**
     * the milli seconds of a minute 60 * 1000
     */
    public static final long MINUTEMILLI = 60000;
    /**
     * the milli seconds of a second
     */
    public static final long SECONDMILLI = 1000;
    /**
     * added time
     */
    public static final String TIMETO = " 23:59:59";
    /**
     * flag before
     */
    public static final int BEFORE = 1;
    /**
     * flag after
     */
    public static final int AFTER = 2;
    /**
     * flag equal
     */
    public static final int EQUAL = 3;
    /**
     * date format dd/MMM/yyyy:HH:mm:ss +0900
     */
    public static final String TIME_PATTERN_LONG = "dd/MMM/yyyy:HH:mm:ss +0900";
    /**
     * date format dd/MM/yyyy:HH:mm:ss +0900
     */
    public static final String TIME_PATTERN_LONG2 = "dd/MM/yyyy:HH:mm:ss +0900";
    /**
     * date format dd/MMM/yyyy:HH:mm:ss
     */
    public static final String TIME_PATTERN_LONG3 = "dd/MMM/yyyy:HH:mm:ss";
    /**
     * date format YYYY-MM-DD HH24:MI:SS
     */
    public static final String DB_TIME_PATTERN = "YYYY-MM-DD HH24:MI:SS";
    /**
     * date format YYYYMMDDHH24MISS
     */
    public static final String DB_TIME_PATTERN_1 = "YYYYMMDDHH24MISS";
    /**
     * date format dd/MM/yy HH:mm:ss
     */
    public static final String TIME_PATTERN_SHORT = "dd/MM/yy HH:mm:ss";
    /**
     * date format dd/MM/yy HH24:mm
     */
    public static final String TIME_PATTERN_SHORT_1 = "yyyy/MM/dd HH:mm";
    /**
     * date format yyyy年MM月dd日 HH:mm:ss
     */
    public static final String TIME_PATTERN_SHORT_2 = "yyyy年MM月dd日 HH:mm:ss";
    /**
     * date format yyyyMMddHHmmss
     */
    public static final String TIME_PATTERN_SESSION = "yyyyMMddHHmmss";
    /**
     * date format yyyyMMddHHmmssSSS
     */
    public static final String TIME_PATTERN_MILLISECOND = "yyyyMMddHHmmssSSS";
    /**
     * date format yyyyMMdd
     */
    public static final String DATE_FMT_0 = "yyyyMMdd";
    /**
     * date format yyyy/MM/dd
     */
    public static final String DATE_FMT_1 = "yyyy/MM/dd";
    /**
     * date format yyyy/MM/dd hh:mm:ss
     */
    public static final String DATE_FMT_2 = "yyyy/MM/dd hh:mm:ss";
    /**
     * date format yyyy-MM-dd
     */
    public static final String DATE_FMT_3 = "yyyy-MM-dd";
    /**
     * date format yyyy年MM月dd日
     */
    public static final String DATE_FMT_4 = "yyyy年MM月dd日";
    /**
     * date format yyyy-MM-dd HH
     */
    public static final String DATE_FMT_5 = "yyyy-MM-dd HH";
    /**
     * date format yyyy-MM
     */
    public static final String DATE_FMT_6 = "yyyy-MM";
    /**
     * date format MM月dd日 HH:mm
     */
    public static final String DATE_FMT_7 = "MM月dd日 HH:mm";
    /**
     * date format MM月dd日 HH:mm
     */
    public static final String DATE_FMT_8 = "HH:mm:ss";
    /**
     * date format MM月dd日 HH:mm
     */
    public static final String DATE_FMT_9 = "yyyy.MM.dd";
    public static final String DATE_FMT_10 = "HH:mm";
    public static final String DATE_FMT_11 = "yyyy.MM.dd HH:mm:ss";
    /**
     * date format yyyy年MM月dd日
     */
    public static final String DATE_FMT_12 = "MM月dd日";
    public static final String DATE_FMT_13 = "yyyy年MM月dd日HH时mm分";
    public static final String DATE_FMT_14 = "yyyyMM";
    public static final String DATE_FMT_15 = "MM-dd HH:mm:ss";
    public static final String DATE_FMT_16 = "yyyyMMddHHmm";
    public static final String DATE_FMT_17 = "HHmmss";
    public static final String DATE_FMT_18 = "yyyy";
    /**
     * date format yyyy-MM-dd HH:mm:ss
     */
    public static final String TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    /**
     * date format yyyy-MM-dd HH:mm:ss.SSS
     */
    public static final String TIME_PATTERN_MSEL = "yyyy-MM-dd HH:mm:ss.SSS";

    //默认使用系统当前时区
    private static final ZoneId ZONE = ZoneId.systemDefault();

    /**
     * Date 转 自定义格式string
     *
     * @param date   时间
     * @param format 例：yyyy-MM-dd hh:mm:ss
     * @return 日期字符串
     */
    public static String formatDateToString(Date date, String format) {
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        return formatLocalDateTimeToString(localDateTime, format);
    }

    /**
     * localDateTime 转 自定义格式string
     *
     * @param localDateTime 时间
     * @param format        例：yyyy-MM-dd hh:mm:ss
     * @return 日期字符串
     */
    @SneakyThrows(DateTimeParseException.class)
    public static String formatLocalDateTimeToString(LocalDateTime localDateTime, String format) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return localDateTime.format(formatter);
    }

    /**
     * 将日期字符串转LocalDateTime
     *
     * @param dateStr 例："2017-08-11 01:00:00"
     * @param format  例："yyyy-MM-dd HH:mm:ss"
     * @return 日期
     */
    @SneakyThrows(DateTimeParseException.class)
    public static LocalDateTime stringToLocalDateTime(String dateStr, String format) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return LocalDateTime.parse(dateStr, formatter);
    }

    /**
     * 将日期字符串转LocalDate
     *
     * @param dateStr 例："2017-08-11"
     * @param format  例："yyyy-MM-dd"
     * @return 日期
     */
    @SneakyThrows(DateTimeParseException.class)
    public static LocalDate stringToLocalDate(String dateStr, String format) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return LocalDate.parse(dateStr, formatter);
    }

    /**
     * 将日期字符串转Date
     *
     * @param dateStr 例："2017-08-11"
     * @param format  例："yyyy-MM-dd"
     * @return 日期
     */
    @SneakyThrows(DateTimeParseException.class)
    public static Date stringToDate(String dateStr, String format) {
        return localDateTimeToDate(stringToLocalDateTime(dateStr, format));
    }

    /**
     * localDateTime 转化成date
     *
     * @param localDateTime
     * @return
     */
    public static Date localDateTimeToDate(LocalDateTime localDateTime) {
        if (null == localDateTime) {
            return null;
        }
        Instant instant = localDateTime.atZone(ZONE).toInstant();
        return Date.from(instant);
    }

    /**
     * timestamp 转化成date
     *
     * @param timestamp
     * @return
     */
    public static LocalDateTime getDateTimeOfTimestamp(String timestamp) {
        long timestampLong = Long.parseLong(timestamp);
        Instant instant = Instant.ofEpochMilli(timestampLong);
        ZoneId zone = ZoneId.systemDefault();
        return LocalDateTime.ofInstant(instant, zone);
    }

    /**
     * timestamp 转化成date 日期格式（yyyy-MM-dd hh:mm:ss）
     *
     * @param timestamp
     * @return
     */
    public static String getTimestampToDate(long timestamp) {
        return timestamp != 0 ?
                DateUtil.formatLocalDateTimeToString(DateUtil.getDateTimeOfTimestamp(String.valueOf(timestamp)),
                        TIME_PATTERN) : null;
    }

    /**
     * localDate 转化成date
     *
     * @param localDate
     * @return
     */
    public static Date localDateToDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay(ZONE).toInstant());
    }

    /**
     * 根据时间获取当月有多少天数
     *
     * @param date
     * @return
     */
    public static int getActualMaximum(Date date) {
        return dateToLocalDateTime(date).getMonth().length(dateToLocalDate(date).isLeapYear());
    }

    /**
     * 根据日期获得星期
     *
     * @param date
     * @return 1:星期一；2:星期二；3:星期三；4:星期四；5:星期五；6:星期六；7:星期日；
     */
    public static int getWeekOfDate(Date date) {
        return dateToLocalDateTime(date).getDayOfWeek().getValue();
    }


    /**
     * 计算两个日期LocalDate相差的天数，不考虑日期前后，返回结果>=0
     *
     * @param before
     * @param after
     * @return 天数
     */
    public static int getAbsDateDiffDay(LocalDate before, LocalDate after) {
        return Math.abs(Period.between(before, after).getDays());
    }

    /**
     * 计算两个时间LocalDateTime相差的天数，不考虑日期前后，返回结果>=0
     *
     * @param before
     * @param after
     * @return
     */
    public static int getAbsTimeDiffDay(LocalDateTime before, LocalDateTime after) {
        return Math.abs(Period.between(before.toLocalDate(), after.toLocalDate()).getDays());
    }

    /**
     * 计算两个时间LocalDateTime相差的月数，不考虑日期前后，返回结果>=0
     *
     * @param before
     * @param after
     * @return
     */
    public static int getAbsTimeDiffMonth(LocalDateTime before, LocalDateTime after) {
        return Math.abs(Period.between(before.toLocalDate(), after.toLocalDate()).getMonths());
    }

    /**
     * 计算两个时间LocalDateTime相差的年数，不考虑日期前后，返回结果>=0
     *
     * @param before 开始时间
     * @param after  终止时间
     * @return int
     */
    public static int getAbsTimeDiffYear(LocalDateTime before, LocalDateTime after) {
        return Math.abs(Period.between(before.toLocalDate(), after.toLocalDate()).getYears());
    }

    /**
     * 获取两个时间相差天数
     *
     * @param before 开始时间
     * @param after  结束时间
     * @return int  相差天数
     */
    public static int getTimeDiffDay(LocalDate before, LocalDate after) {
        return (int) Math.abs(ChronoUnit.DAYS.between(before, after));
    }

    /**
     * 获取两个时间相差天数
     *
     * @param before 开始时间
     * @param after  结束时间
     * @return int  相差天数
     */
    public static int getTimeDiffDay(LocalDateTime before, LocalDateTime after) {
        return (int) Math.abs(ChronoUnit.DAYS.between(before, after));
    }

    /**
     * 获取两个时间相差月数
     *
     * @param before 开始时间
     * @param after  结束时间
     * @return int  相差月数
     */
    public static int getTimeDiffMonth(LocalDateTime before, LocalDateTime after) {

        return (int) Math.abs(ChronoUnit.MONTHS.between(before, after));
    }

    /**
     * @param before 开始时间
     * @param after  结束时间
     * @return int 相差年数
     */
    public static int getTimeDiffYear(LocalDateTime before, LocalDateTime after) {

        return (int) Math.abs(ChronoUnit.YEARS.between(before, after));
    }

    /**
     * 获取指定日期的当月的月份数
     *
     * @param date 日期
     * @return int  月份天数
     */
    public static int getLastMonth(Date date) {
        return dateToLocalDateTime(date).getMonth().getValue();
    }


    /**
     * 特定日期的当月第一天
     *
     * @param date
     * @return
     */
    public static LocalDate newThisMonth(Date date) {
        LocalDate localDate = dateToLocalDate(date);
        return LocalDate.of(localDate.getYear(), localDate.getMonth(), 1);
    }

    /**
     * 特定日期的当月最后一天
     *
     * @param date
     * @return
     */
    public static LocalDate lastThisMonth(Date date) {
        int lastDay = getActualMaximum(date);
        LocalDate localDate = dateToLocalDate(date);
        return LocalDate.of(localDate.getYear(), localDate.getMonth(), lastDay);
    }


    /**
     * 特定日期的当年第一天
     *
     * @param date
     * @return
     */
    public static LocalDate newThisYear(Date date) {
        LocalDate localDate = dateToLocalDate(date);
        return LocalDate.of(localDate.getYear(), 1, 1);

    }


    public static Timestamp getCurrentDateTime() {
        return new Timestamp(Instant.now().toEpochMilli());

    }

    /**
     * 获取当前时间
     *
     * @return LocalDateTime
     */
    public static LocalDateTime getCurrentLocalDateTime() {
        return LocalDateTime.now(Clock.system(ZoneId.of("Asia/Shanghai")));

    }


    /**
     * 修改日期时间的时间部分
     *
     * @param date
     * @param customTime 必须为"hh:mm:ss"这种格式
     */
    public static LocalDateTime reserveDateCustomTime(Date date, String customTime) {
        String dateStr = dateToLocalDate(date).toString() + " " + customTime;
        return stringToLocalDateTime(dateStr, TIME_PATTERN);
    }

    /**
     * 修改日期时间的时间部分
     *
     * @param date
     * @param customTime 必须为"hh:mm:ss"这种格式
     */
    public static LocalDateTime reserveDateCustomTime(Timestamp date, String customTime) {
        String dateStr = timestampToLocalDate(date).toString() + " " + customTime;
        return stringToLocalDateTime(dateStr, TIME_PATTERN);
    }

    /**
     * 把日期后的时间归0 变成(yyyy-MM-dd 00:00:00:000)
     *
     * @param date
     * @return LocalDateTime
     */
    public static LocalDateTime zerolizedTime(Date date) {
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        return LocalDateTime.of(localDateTime.getYear(), localDateTime.getMonth(), localDateTime.getDayOfMonth(),
                0, 0, 0, 0);

    }

    /**
     * 把时间变成(yyyy-MM-dd 00:00:00:000)
     *
     * @param date
     * @return LocalDateTime
     */
    public static LocalDateTime zerolizedTime(Timestamp date) {
        LocalDateTime localDateTime = timestampToLocalDateTime(date);
        return LocalDateTime.of(localDateTime.getYear(), localDateTime.getMonth(), localDateTime.getDayOfMonth(),
                0, 0, 0, 0);
    }

    /**
     * 把日期的时间变成(yyyy-MM-dd 23:59:59:999)
     *
     * @param date
     * @return LocalDateTime
     */
    public static LocalDateTime getEndTime(Date date) {
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        return LocalDateTime.of(localDateTime.getYear(), localDateTime.getMonth(), localDateTime.getDayOfMonth(),
                23, 59, 59, 999 * 1000000);
    }

    /**
     * 把时间变成(yyyy-MM-dd 23:59:59:999)
     *
     * @param date
     * @return LocalDateTime
     */
    public static LocalDateTime getEndTime(Timestamp date) {
        LocalDateTime localDateTime = timestampToLocalDateTime(date);
        return LocalDateTime.of(localDateTime.getYear(), localDateTime.getMonth(),
                localDateTime.getDayOfMonth(), 23, 59, 59, 999 * 1000000);
    }

    /**
     * 计算特定时间到 当天 23.59.59.999 的秒数
     *
     * @return
     */
    public static int calculateToEndTime(Date date) {
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime end = getEndTime(date);
        return (int) (end.toEpochSecond(ZoneOffset.UTC) - localDateTime.toEpochSecond(ZoneOffset.UTC));
    }

    /**
     * 获取前一天 的年
     *
     * @return
     */
    public static String getYear() {

        return DateUtil.formatLocalDateTimeToString(LocalDateTime.now().plusDays(-1), DATE_FMT_18);
    }

    /**
     * 获取前一天 的年-月
     *
     * @return
     */
    public static String getMouth() {

        return DateUtil.formatLocalDateTimeToString(LocalDateTime.now().plusDays(-1), DATE_FMT_6);
    }

    /**
     * 获取前一天 的  年-月-日
     *
     * @return
     */
    public static String getDay() {

        return DateUtil.formatLocalDateTimeToString(LocalDateTime.now().plusDays(-1), DateUtil.DATE_FMT_3);
    }

    /**
     * 增加或减少年/月/周/天/小时/分/秒数
     *
     * @param localDateTime 例：ChronoUnit.DAYS
     * @param chronoUnit
     * @param num
     * @return LocalDateTime
     */
    public static LocalDateTime addTime(LocalDateTime localDateTime, ChronoUnit chronoUnit, int num) {
        return localDateTime.plus(num, chronoUnit);
    }

    /**
     * 增加或减少年/月/周/天/小时/分/秒数
     *
     * @param chronoUnit 例：ChronoUnit.DAYS
     * @param num
     * @return LocalDateTime
     */
    public static LocalDateTime addTime(Date date, ChronoUnit chronoUnit, int num) {
        long nanoOfSecond = (date.getTime() % 1000) * 1000000;
        LocalDateTime localDateTime = LocalDateTime.ofEpochSecond(date.getTime() / 1000,
                (int) nanoOfSecond, ZoneOffset.of("+8"));
        return localDateTime.plus(num, chronoUnit);
    }

    /**
     * 增加或减少年/月/周/天/小时/分/秒数
     *
     * @param chronoUnit 例：ChronoUnit.DAYS
     * @param num
     * @return LocalDateTime
     */
    public static LocalDateTime addTime(Timestamp date, ChronoUnit chronoUnit, int num) {
        long nanoOfSecond = (date.getTime() % 1000) * 1000000;
        LocalDateTime localDateTime = LocalDateTime.ofEpochSecond(date.getTime() / 1000,
                (int) nanoOfSecond, ZoneOffset.of("+8"));
        return localDateTime.plus(num, chronoUnit);
    }

    /**
     * Date 转 LocalDateTime
     *
     * @param date
     * @return LocalDateTime
     */
    public static LocalDateTime dateToLocalDateTime(Date date) {
        long nanoOfSecond = (date.getTime() % 1000) * 1000000;
        return LocalDateTime.ofEpochSecond(date.getTime() / 1000, (int) nanoOfSecond, ZoneOffset.of("+8"));
    }

    /**
     * Timestamp 转 LocalDateTime
     *
     * @param date
     * @return LocalDateTime
     */
    public static LocalDateTime timestampToLocalDateTime(Timestamp date) {
        return LocalDateTime.ofEpochSecond(date.getTime() / 1000, date.getNanos(), ZoneOffset.of("+8"));
    }

    /**
     * Date 转 LocalDateTime
     *
     * @param date
     * @return LocalDate
     */
    public static LocalDate dateToLocalDate(Date date) {

        return dateToLocalDateTime(date).toLocalDate();
    }

    /**
     * timestamp 转 LocalDateTime
     *
     * @param date
     * @return LocalDate
     */
    public static LocalDate timestampToLocalDate(Timestamp date) {

        return timestampToLocalDateTime(date).toLocalDate();
    }

    /**
     * 比较两个LocalDateTime是否同一天
     *
     * @param begin
     * @param end
     * @return
     */
    public static boolean isTheSameDay(LocalDateTime begin, LocalDateTime end) {
        return begin.toLocalDate().equals(end.toLocalDate());
    }


    /**
     * 比较两个时间LocalDateTime大小
     *
     * @param time1
     * @param time2
     * @return 1:第一个比第二个大；0：第一个与第二个相同；-1：第一个比第二个小
     */
    public static int compareTwoTime(LocalDateTime time1, LocalDateTime time2) {

        if (time1.isAfter(time2)) {
            return 1;
        } else if (time1.isBefore(time2)) {
            return -1;
        } else {
            return 0;
        }
    }

    /**
     * 比较time1,time2两个时间相差的微秒数，如果time1<=time2,返回0
     *
     * @param time1
     * @param time2
     */
    public static long getTwoTimeDiffMillisecond(Timestamp time1, Timestamp time2) {
        long diff = time1.getTime() - time2.getTime();
        return diff > 0 ? diff : 0;
    }

    /**
     * 比较time1,time2两个时间相差的秒数，如果time1<=time2,返回0
     *
     * @param time1
     * @param time2
     */
    public static long getTwoTimeDiffSecond(Timestamp time1, Timestamp time2) {
        long diff = timestampToLocalDateTime(time1).toEpochSecond(ZoneOffset.UTC) - timestampToLocalDateTime(time2)
                .toEpochSecond(ZoneOffset.UTC);
        return diff > 0 ? diff : 0;
    }

    /**
     * 比较time1,time2两个时间相差的分钟数，如果time1<=time2,返回0
     *
     * @param time1
     * @param time2
     */
    public static long getTwoTimeDiffMin(Timestamp time1, Timestamp time2) {
        long diff = getTwoTimeDiffSecond(time1, time2) / 60;
        return diff > 0 ? diff : 0;
    }

    /**
     * 比较time1,time2两个时间相差的小时数，如果time1<=time2,返回0
     *
     * @param time1 开始时间
     * @param time2 结束时间
     */
    public static long getTwoTimeDiffHour(Timestamp time1, Timestamp time2) {
        long diff = getTwoTimeDiffSecond(time1, time2) / 3600;
        return diff > 0 ? diff : 0;
    }

    /**
     * 判断当前时间是否在时间范围内
     *
     * @param startTime 起始时间
     * @param endTime   结束时间
     * @return 是与否
     */
    public static boolean isTimeInRange(Date startTime, Date endTime) {
        LocalDateTime now = getCurrentLocalDateTime();
        LocalDateTime start = dateToLocalDateTime(startTime);
        LocalDateTime end = dateToLocalDateTime(endTime);
        return (start.isBefore(now) && end.isAfter(now)) || start.isEqual(now) || end.isEqual(now);
    }

    /**
     * 获取时间范围内的周的起止日期
     *
     * @param beginDate 起始时间
     * @param endDate   结束时间
     * @return
     */
    public static List getStartAndEndOfWeek(Date beginDate, Date endDate) {
        return getStartAndEndOfWeek(dateToLocalDate(beginDate), dateToLocalDate(endDate));
    }

    /**
     * 获取时间范围内的周的起止日期
     *
     * @param beginDate 起始时间
     * @param endDate   结束时间
     * @return
     */
    @SuppressWarnings("unchecked")
    public static List getStartAndEndOfWeek(LocalDate beginDate, LocalDate endDate) {
        int weekday = beginDate.getDayOfWeek().getValue();
        LocalDate startOfWeek = beginDate;
        if (weekday != 1) {
            startOfWeek = beginDate.plusDays(-(weekday - 1));
        }
        LocalDate endOfWeek = startOfWeek.plusDays(6);

        Map<String, Object> weekDates = Maps.newHashMap();
        weekDates.put("startOfWeek", startOfWeek);
        weekDates.put("endOfWeek", endOfWeek);

        List list = Lists.newArrayList();
        list.add(weekDates);

        while (endOfWeek.isBefore(endDate)) {
            startOfWeek = startOfWeek.plusDays(7);//下个周一
            endOfWeek = startOfWeek.plusDays(6);//下个周末

            Map<String, Object> map = Maps.newHashMap();
            map.put("startOfWeek", startOfWeek);
            map.put("endOfWeek", endOfWeek);
            list.add(map);
        }
        return list;
    }

    /**
     * 将 date 进行 格式化
     *
     * @param date
     * @param format
     */
    public static Date formatDate(Date date, String format) {
        String str = formatDateToString(date, format);
        LocalDate localDate = stringToLocalDate(str, format);
        return null != localDate ? localDateToDate(localDate) : null;
    }

    /**
     * 转换格式
     *
     * @param timestamp
     * @return bool
     */
    public static String dateAndDate(String timestamp) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(TIME_PATTERN_LONG3, Locale.ENGLISH);
        DateTimeFormatter formatterRn = DateTimeFormatter.ofPattern(TIME_PATTERN);
        return formatterRn.format(formatter.parse(timestamp));
    }

    /**
     * 字符串转时戳
     *
     * @param timestamp
     * @return Timestamp
     */
    public static Timestamp strToTimestamp(String timestamp) {
        return new Timestamp(Long.parseLong(timestamp));
    }


    /**
     * 获取Unix时间戳
     *
     * @return long
     */
    public static long getUnixTimestamp() {
        return Instant.now().getEpochSecond();
    }
}
