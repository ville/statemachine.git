package com.zc.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author :  ville
 * @description : 从Spring容器中获取bean
 * @CreateTime : 2019-07-05 14:43
 * @Version : 1.0.0
 * @Copyright : http://www.8atour.com
 * @modify : ville
 **/
@Component
public class BeanHelper implements ApplicationContextAware {

    private static ApplicationContext applicationContext = null;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        BeanHelper.applicationContext = applicationContext;
    }

    public BeanHelper getInstance() {
        return new BeanHelper();
    }

    public static ApplicationContext getAppContext() {

        return applicationContext;
    }

    public static Object getBean(@NotBlank String name) {
        return getAppContext().getBean(name);
    }

    public static <T> T getBean(@NotNull Class<T> t) {
        return getAppContext().getBean(t);
    }
}
