package com.zc.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;

import javax.annotation.Resource;
import java.util.Optional;

/**
 * @author :  ville
 * @description : 获取yml或者properties文件中属性值
 * @CreateTime : 2019-07-05 14:51
 * @Version : 1.0.0
 * @Copyright : http://www.8atour.com
 * @modify : ville
 **/
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConfigUtil {
    @Resource
    private static final Environment env;

    static {
        env = BeanHelper.getBean(Environment.class);
    }

    public static boolean getBoolean(String paramString) {
        return Boolean.parseBoolean(env.getProperty(paramString, "false"));
    }

    public static int getInt(String paramString) {
        String property = getString(paramString);
        return Optional.ofNullable(property).map(p->Integer.valueOf(property.trim())).orElse(0);
    }

    public static int getInt(String paramString, int defaultValue) {
        String property = getString(paramString);
        return Optional.ofNullable(property).map(p->Integer.valueOf(property.trim())).orElse(defaultValue);
    }

    public static double getDouble(String paramString, double defaultValue) {
        String property = getString(paramString);
        return Optional.ofNullable(property).map(p->Double.valueOf(property.trim())).orElse(defaultValue);
    }

    public static long getLong(String paramString) {
        String property = getString(paramString);
        return Optional.ofNullable(property).map(p->Long.valueOf(property.trim())).orElse(0L) ;
    }

    private static String getString(String paramString) {
        return env.getProperty(paramString);
    }

    public static String getString(String paramString, String defaultValue) {
        String property = getString(paramString);
        return Optional.ofNullable(property).map(p ->property.trim()).orElse(defaultValue);

    }

    public static String[] getStringArray(String paramString) {
        String property = getString(paramString);
        return new String[]{Optional.ofNullable(property).map(p -> property.trim())
                .orElse(null)};
    }
}


