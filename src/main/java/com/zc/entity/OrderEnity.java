package com.zc.entity;

import com.zc.enums.OrderStatusEnum;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author : ville
 * @version : 1.0.0
 * @description : 订单实体
 * @createTime : 2020/6/9-15:18
 * @copyright : ville@126.com
 * @modify : ville
 **/
@Entity
@Table(name = "t_order")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OrderEnity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",length =10,nullable = false)
    private Integer id;

    @NotNull
    @Column(name = "order_id",length = 10,nullable = false)
    private Integer orderId;

    @NotNull
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "order_status",length = 2)
    private OrderStatusEnum status;
}
