package com.zc.enums;

import lombok.ToString;

/**
 * @author : ville
 * @version :1.0.0
 * @description : 订单状态枚举
 * @createTime :2020/6/9-15:22
 * @copyright : villebo@126.com
 * @modify : ville
 **/
@ToString
public enum OrderStatusEnum {
    /**
     * 待支付
     */
    WAIT_PAYMENT,
    /**
     * 待发货
     */
    WAIT_DELIVER,
    /**
     * 待收货
     */
    WAIT_RECEIVE,
    /**
     * 订单完成
     */
    FINISH;
}
