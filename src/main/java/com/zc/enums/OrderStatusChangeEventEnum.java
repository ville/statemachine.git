package com.zc.enums;

import lombok.ToString;

/**
 * @author : ville
 * @version : 1.0.0
 * @description : 订单状态改变事件枚举
 * @createTime :2020/6/9-16:17
 * @copyright : villebo@126.com
 * @modify : ville
 **/
@ToString
public enum OrderStatusChangeEventEnum {
    /**
     * 支付
     */
    PAYED,
    /**
     * 发货
     */
    DELIVERY,
    /**
     * 确认收货
     */
    RECEIVED;
}
