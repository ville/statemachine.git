package com.zc.repository;

import com.zc.entity.OrderEnity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author : ville
 * @version : 1.0.0
 * @description : 订单查询接口
 * @createTime : 2020/6/9-16:51
 * @copyright : villebo@126.com
 * @modify : ville
 **/

public interface OrderRepository extends JpaRepository<OrderEnity, Integer> {
    OrderEnity findByOrderId(Integer order);
}
