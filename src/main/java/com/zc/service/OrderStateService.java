package com.zc.service;

import com.zc.entity.OrderEnity;
import com.zc.enums.OrderStatusChangeEventEnum;
import com.zc.handler.PersistStateMachineHandler;
import com.zc.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author : ville
 * @version :1.0.0
 * @description : 订单服务
 * @createTime : 2020/6/9-17:35
 * @copyright : villebo@126.com
 * @modify : ville
 **/
@Component
public class OrderStateService {

    private final PersistStateMachineHandler handler;

    public OrderStateService(PersistStateMachineHandler handler) {
        this.handler = handler;
    }

    @Autowired
    private OrderRepository orderRepository;

    public List<OrderEnity> getOrderList() {
        return orderRepository.findAll();
    }

    public boolean change(int order, OrderStatusChangeEventEnum event) {
        OrderEnity o = orderRepository.findByOrderId(order);
        return handler.handleEventWithState(MessageBuilder.withPayload(event)
                .setHeader("order", order).build(), o.getStatus());
    }
}
