package com.zc.config;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * @author : ville
 * @version : 1.0.0
 * @description : 配置数据库连接池
 * @createTime : 2020/6/9-11:56
 * @copyright : ville@126.com
 * @modify : ville
 **/
@Configuration
public class DruidPoolConfig {

    @ConfigurationProperties(prefix = "spring.datasource.druid")
    public DataSource dataSource() {
        return DruidDataSourceBuilder.create().build();
    }
}
