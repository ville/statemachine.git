package com.zc.config.order;

import com.zc.enums.OrderStatusChangeEventEnum;
import com.zc.enums.OrderStatusEnum;
import com.zc.handler.PersistStateMachineHandler;
import com.zc.listener.OrderPersistStateChangeListener;
import com.zc.service.OrderStateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.StateMachine;

/**
 * @author : ville
 * @version : 1.0.0
 * @description : 订单持久化配置
 * @createTime : 2020/6/9-17:30
 * @copyright : villebo@126.com
 * @modify : ville
 **/
@Configuration
public class OrderPersistHandlerConfig {

    @Autowired
    private StateMachine<OrderStatusEnum, OrderStatusChangeEventEnum> stateMachine;


    @Bean
    public OrderStateService persist() {
        PersistStateMachineHandler handler = persistStateMachineHandler();
        handler.addPersistStateChangeListener(persistStateChangeListener());
        return new OrderStateService(handler);
    }

    @Bean
    public PersistStateMachineHandler persistStateMachineHandler() {
        return new PersistStateMachineHandler(stateMachine);
    }

    @Bean
    public OrderPersistStateChangeListener persistStateChangeListener(){
        return new OrderPersistStateChangeListener();
    }
}
